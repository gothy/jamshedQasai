<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Comment;
use App\Admin;
use App\Image;
use Elasticquent\ElasticquentTrait;


class Blog extends Model
{
    use ElasticquentTrait;

    protected $mappingProperties = array(

        'title' => [
            'type' => 'text',
            "analyzer" => "standard",
        ],
        'content' => [
            'type' => 'text',
            "analyzer" => "standard",
        ],
    );

        public function blogComment(){
            return $this->hasMany(Comment::class,'blog_id');
        }

        public function postedBy(){
            return $this->belongsTo(Admin::class,'admin_id');
        }

        public function imagesOfPost(){
            return $this->hasMany(Image::class);
        }
}
