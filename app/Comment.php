<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Blog;

class Comment extends Model
{

    public function commentFromUser(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function commentOnPost(){
        return $this->belongsTo(Blog::class,'blog_id');
    }

}
