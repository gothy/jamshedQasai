<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Blog;

class Image extends Model
{
    public function imageOfPost(){
        return $this->belongsTo(Blog::class,'blog_id');
    }
}
