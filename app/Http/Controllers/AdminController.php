<?php

namespace App\Http\Controllers;

use App\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Symfony\Component\VarDumper\VarDumper;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class AdminController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view ('admin_UI.panel');
    }

    public function viewAdmins()
    {
        $admins = Admin::all();
        return view ('admins.index',compact('admins'));
    }

    public function addAdmin(){
        return view ('admins.addAdmin');
    }

    public function registerAdmin(Request $request)
    {
        $name = $request->name;
        $password = $request->password;
        $email = $request->email;

        $newAdmin = new Admin();

        $newAdmin->name = $name;
        $newAdmin->email = $email;
        $newAdmin->password = Hash::make($password);
        $newAdmin->created_at = now();
        $newAdmin->updated_at = now();
        $newAdmin->save();

        return Redirect::route('viewAdmins');

    }

    public function editAdmin($id)
    {

        $admin = Admin::find($id);
        return view('admins.edit',compact('admin'));

    }

    public function updateAdmin(Request $request)
    {
        $admin = Admin::find($request->adminId);
        $name = $request->name;
        $email = $request->email;

        $admin->name = $name;
        $admin->email = $email;
        $admin->save();

        return Redirect::route('viewAdmins');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewRoles()
    {
        $roles = Role::all();
        return view('roles.index',compact('roles'));
    }

    public function newRole(Request $request)
    {
        $permissions = Permission::all();
        return view('roles.addRole',compact('permissions'));
    }

    public function addRole(Request $request)
    {

        $this->validate($request, [
                'name'=>'required',
                'permission' =>'required',
            ]
        );

        $name = $request->name;

        $newRole = new Role();

        $newRole->name = $name;
        $newRole->guard_name = 'admin';
        $newRole->created_at = now();
        $newRole->updated_at = now();
        $newRole->save();

        $permissions = Permission::find($request->permission);

        if(isset($permissions))
        {
            foreach ($permissions as $permission) {

                $p = Permission::find($permission);
                //Fetch the newly created role and assign permission
                $role = Role::where('name', '=', $name)->first();
                $role->givePermissionTo($p);
            }
        }

        return Redirect::route('view_Roles');
    }

    public function editRole($id)
    {
        $role = Role::find($id);
        $permissions = Permission::all();
        return view('roles.edit',compact('role','permissions'));

    }

    public function updateRole(Request $request)
    {

//        return $request->all();
        $this->validate($request, [
                'name'=>'required',
                'permission' =>'required',
            ]
        );

        $name = $request['name'];
        $role = Role::find($request->roleId);
        $role->name = $name;

        $role->update();


        $p_all = Permission::all();//Get all permissions

        foreach ($p_all as $p) {
            $role->revokePermissionTo($p); //Remove all permissions associated with role
        }
        $permissions = Permission::find($request->permission);
        if(isset($permissions))
        {
            foreach ($permissions as $permission) {

                $p = Permission::find($permission);
                //Fetch the newly created role and assign permission
                $role = Role::where('name', '=', $name)->first();
                $role->givePermissionTo($p);
            }
        }
        return Redirect::route('view_Roles');
    }


    public function assignRole($id){

        $admin = Admin::find($id);
        $roles = Role::all();
        return view('admins.assignRole',compact('admin','roles'));
    }

    public function updateAssignedRole(Request $request)
    {
        $admin = Admin::find($request->adminId);
        $roles = $request->role;

        $admin->roles()->detach();

        if(isset($roles))
        {
            foreach($roles as $role)
            {
                $role_name = Role::where('id','=',$role)->firstOrFail();
                $admin->assignRole($role_name);
            }
        }

        return Redirect::route('viewAdmins');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $admin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $admin)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {
        //
    }
}
