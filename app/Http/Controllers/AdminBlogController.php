<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
//use Illuminate\Support\Facades\Session;
use Session;
use App\Image;
use Zend\Diactoros\Response;
use Spatie\Analytics\Period;
use App\Libraries\GoogleAnalytics;
use Analytics;

class AdminBlogController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $result = GoogleAnalytics::topCountries();
        $country = $result->pluck('country');
        $country_sessions = $result->pluck('sessions');
        return view('admin_UI.panel',compact('country', 'country_sessions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function blogCreator()
    {
        return view ('admin_UI.addblog');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        if(Input::hasfile('image'))

        $file=Input::file('image');
        $fileName=str_random().$file->getClientOriginalName();

        $file->move('uploads/', $fileName);

        $detail=$request->summernoteInput;

        $dom = new \domdocument();
        libxml_use_internal_errors(true);
        $dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        $images = $dom->getelementsbytagname('img');

        foreach($images as $k => $img){
            $data = $img->getattribute('src');

            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);

            $data = base64_decode($data);
            $image_name= time().$k.'.png';
            $path = public_path() .'/uploads/'. $image_name;

            file_put_contents($path, $data);

            $img->removeattribute('src');
            $img->setattribute('src','/uploads/'. $image_name);
        }

        $detail = $dom->savehtml();
        $newBlog = new Blog;
        $newBlog->title = $request->title;
        $newBlog->admin_id = Auth::user()->id;
        $newBlog->content = $detail;
        $newBlog->path = 'uploads/'.$fileName;
        $newBlog->save();
        Blog::deleteIndex();
        Blog::addAllToIndex();
        $blog_id = $newBlog->id;
        return view('admin_UI.addImages',compact('blog_id'));
    }

    public function updateMultipleImages(Request $request)
        {
            $blog_id = $request->blog_id;

            if(Input::hasfile('image'))
            {
                $file=Input::file('image');
                $fileName=str_random().$file->getClientOriginalName();
                $file->move('uploads/', $fileName);
                $path = 'uploads/'.$fileName;
                $images = array();
                $images['image'] = $path;
                Session::push('images', $images);
            }
                $images = $request->session()->get('images');
                if(isset($images)) {
                    foreach ($images as $image) {
                        $gallery = new Image();
                        $gallery->blog_id = $blog_id;
                        $gallery->path = $image['image'];
                        $gallery->save();
                    }
                    session()->forget('images');
                }
            return Redirect::route('allPosts');
        }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $allBlogs = Blog::all();
        return view ('admin_UI.allPosts', compact('allBlogs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = Blog::find($id);
        return view('admin_UI.editblog', compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $Blog = Blog::find($request->blogId);
        if(Input::hasfile('pic'))
        {
            $this->validate($request,[
                'pic' => 'required|image|mimes:jpeg,png,jpg,gif|max:10000'
            ]);

            $file=Input::file('pic');
            $fileName=str_random().$file->getClientOriginalName();

            $file->move('uploads/', $fileName);
            if (File::exists($Blog->path)) {
                File::delete($Blog->path);
            }
            $Blog->path = 'uploads/'.$fileName;
        }


        $Blog->title = $request->title;
        $Blog->content = $request->summernoteInput;
        $Blog->save();

        return Redirect::route('allPosts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blogId = $id;
        $blog = Blog::find($blogId);

        if (File::exists($blog->path)) {
            File::delete($blog->path);
        }
        $blog->delete();
        return redirect()->back();
    }
}
