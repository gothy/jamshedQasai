<?php
/**
 * Created by PhpStorm.
 * User: Gothy
 * Date: 2/13/2019
 * Time: 5:55 PM
 */

namespace App\Http\Controllers\Api;

use App\Comment;
use App\Blog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


class ApiBlogController
{
    public function showBlogs()
    {
        $blogs = Blog::all();
        return response()->json(['status'=>200,'success'=>$blogs]);
    }

    public function showBlog($id)
    {
        $blogs = Blog::find($id);
        $comments = $blogs->blogComment;
        return response()->json(['status'=>200,'success'=>$blogs]);
    }
    public function addComment(Request $request)
    {
        $comment = new Comment;
        $comment->user_id = Auth::user()->id;
        $comment->blog_id = $request->blog_id;
        $comment->comment = $request->comment;
        $comment->save();
        return response()->json(['status'=>200,'comment'=>$comment]);
    }

}