<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthUserController extends Controller
{

    public function login(Request $request)
    {
        //validate
        $this->validate($request,[
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);
        //attempt to login
        if( Auth::guard('web')->attempt(['email'=>$request->email, 'password'=>$request->password], $request->remember))
        {
            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')-> accessToken;
            return response()->json(['status'=>200,'success'=>$user,'token'=>$success]);
        }
        return response()->json(['status'=>404]);
    }

    public function register(Request $request)
    {
        Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'c_password' => 'required|same:password',
        ]);
        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);
        $success['token'] =  $user->createToken('MyApp')-> accessToken;
        $success['name'] =  $user->name;
        if($user)
        {
            return response()->json(['status'=>200,'success'=>$user]);
        }
        else
            return response()->json(['status'=>404]);

    }

    public function details()
    {
        $user = Auth::user();
        return response()->json(['status'=>200,'success'=>$user]);
    }
}