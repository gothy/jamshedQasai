@extends('layouts.master')

@section('content')
<header class="masthead" style="background-image: url('{{asset('blog_img/img/contact-bg.jpg')}}')">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <div class="page-heading">
                    <h1>Contact Me</h1>
                    <span class="subheading">Have questions? I have answers.</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
            <p>Want to get in touch? Fill out the form below to send me a message and I will get back to you as soon as possible!</p>
            <!-- Contact Form - Enter your email address on line 19 of the mail/contact_me.php file to make this form work. -->
            <!-- WARNING: Some web hosts do not allow emails to be sent through forms to common mail hosts like Gmail or Yahoo. It's recommended that you use a private domain email address! -->
            <!-- To use the contact form, your site must be on a live web host with PHP! The form will not work locally! -->
            <div id="contactForm">
                @csrf
                <div id="alert-success" style="display: none" class="alert alert-success" role="alert">
                    Your Message Has Been Sent <i class="fa fa-check" aria-hidden="true"></i>
                </div>


                <div id="alert-danger" style="display: none" class="alert alert-danger" role="alert">
                    <button class="close" data-dismiss="alert"></button>
                    Error
                </div>

                <div class="control-group">
                    <div class="form-group floating-label-form-group controls">
                        <label>Name</label>
                        <input type="text" class="form-control" placeholder="Name" name="name" id="name" required data-validation-required-message="Please enter your name.">
                        <p class="help-block text-danger"></p>
                    </div>
                </div>
                <div class="control-group">
                    <div class="form-group floating-label-form-group controls">
                        <label>Email Address</label>
                        <input type="email" class="form-control" placeholder="Email Address" name="email" id="email" required data-validation-required-message="Please enter your email address.">
                        <p class="help-block text-danger"></p>
                    </div>
                </div>
                <div class="control-group">
                    <div class="form-group col-xs-12 floating-label-form-group controls">
                        <label>Phone Number</label>
                        <input type="tel" class="form-control" placeholder="Phone Number" id="phone" name="phone" required data-validation-required-message="Please enter your phone number.">
                        <p class="help-block text-danger"></p>
                    </div>
                </div>
                <div class="control-group">
                    <div class="form-group floating-label-form-group controls">
                        <label>Message</label>
                        <textarea rows="5" class="form-control" placeholder="Message" id="message" name="message" required data-validation-required-message="Please enter a message."></textarea>
                        <p class="help-block text-danger"></p>
                    </div>
                </div>
                <br>
                <div id="success"></div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-submit" id="sendMessageButton">Send</button>
                </div>
            </div>
        </div>
    </div>
</div>

<hr>
    @stop

@section('extra_scripts')

    <script>
        $(document).ready(function(){
            $(".btn-submit").click(function(){
                $('.btn-submit').attr('disabled','disabled')
                var token = $("meta[name=csrf-token]").attr('content');
                $.ajax({
                    url: '{{route("send.mail")}}',
                    method: 'post',
                    data: {
                        _token: token,
                        name: $('#name').val(),
                        email: $('#email').val(),
                        phone: $('#phone').val(),
                        message: $('#message').val()
                    },
                    success: function (data) {
                        console.log(data)
                        if(data.status==200){
                            $('#name').val(''),
                            $('#email').val(''),
                            $('#phone').val(''),
                            $('#message').val(''),
                                $('#alert-success').css('display','block').fadeOut(3000),
                                document.getElementById("sendMessageButton").disabled = false;
                        }
                    },
                    error : function(err){
                        console.log(err),
                            $('#alert-danger').css('display','block').fadeOut(3000)
                    }
                })
            })
        });
    </script>

    <script src="{{ asset('blog_js/contactform/js/app.js') }}"></script>
    <script src="{{ asset('blog_js/contactform/js/jquery.validation.min.js') }}"></script>

    @stop