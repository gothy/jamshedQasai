@extends('admin_UI.layouts.master')
@section('custom_head')
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
@stop
@section('content')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container">
                <div class="w3-center"><br>
                    <img src="{{asset('admin_UI/img/img_avatar4.jpeg')}}" alt="Avatar" style="width:30%" class="w3-circle w3-margin-top">
                </div><br>
                <form method="POST" action="{{ route('updateAssignedRole') }}">
                    @csrf
                    <input type="hidden" value="{{$admin->id}}" name="adminId">
                    <div class="form-group row">
                        <label for="role" class="col-md-4 col-form-label text-md-right">Roles</label>
                        <div class="checkbox">
                            <label>
                                @foreach($roles as $role)
                                    <input type="checkbox" value="{{$role->id}}"
                                           name="role[]"
                                           @foreach($admin->roles as $role_per)
                                           @if($role_per->id==$role->id)
                                           checked
                                            @endif
                                            @endforeach>
                                    {{$role->name}}
                                @endforeach
                             </label>
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Update') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop