@extends('admin_UI.layouts.master')

@section('title', '| Users')
@section('content')
    <div class="content-wrapper">
        <div class="content-header">
        <div class="col-lg-12 col-lg-offset-1">
    <h1><i class="fa fa-users"></i> Admin</h1>
    <hr>
<br><br>
            <div class="table-responsive">
        <table class="table table-bordered table-striped text-center">

            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Date/Time Added</th>
                    <th>User Roles</th>
                    <th colspan="2">Operations</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($admins as $admin)
                <tr>
                    <td>{{ $admin->name }}</td>
                    <td>{{ $admin->email }}</td>
                    <td>{{ $admin->created_at->format('F d, Y h:ia') }}</td>
                    @if($admin->roles()->pluck('name')->implode(' ')==='')
                        <td>No Roles Assigned</td>
                        @else
                        <td>{{ $admin->roles()->pluck('name')->implode(' ') }}</td>
                    @endif
                    <td>
                        <a href="{{route('edit_admin',$admin->id)}}"><button type="button"
                                                                             class="btn btn-dark" name="edit">Edit</button></a>
                    </td>
                    <td>
                        <a href="{{route('assign_Role',$admin->id)}}"><button type="button" class="btn btn-info">Assign Role</button></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
        </div>
        </div>
    </div>
@endsection

