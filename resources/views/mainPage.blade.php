@extends('layouts.master')

@section('content')
<style>
    div.results a:hover{
        background-color:#2d995b;
        color:white;
    }
</style>
    <header class="masthead" style="background-image: url('{{asset('blog_img/img/home-bg.jpg')}}')">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <div class="site-heading">
                    <h1>KAAM HO RHA HY</h1>
                    <span class="subheading">SLOWLY SLOWLY</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
                <div class="input-group-append">
                    <input type="text" class="form-control form-control-navbar" id="search" name="search" aria-label="Search"/>
                </div>
            <div id="display" style="width: 100%" class="results"></div>
            @foreach($allBlogs as $Blog)
            <div class="post-preview">
                <a href="{{route('blogPost',$Blog->id)}}">
                    <h2 class="post-title">
                        {{$Blog->title}}
                    </h2>
                    <p>
                        {{strip_tags(substr($Blog->content,0,50))}} .....
                    </p>
                </a>
                <p class="post-meta">Posted by <strong>{{$Blog->postedBy['name']}}</strong>
                    {{$Blog->created_at->diffForHumans()}}</p>
            </div>
            @endforeach
            <hr>
            {{$allBlogs->links()}}
        </div>
    </div>
</div>

<hr>

@stop
@section('extra_scripts')
    <script>
        $(document).ready(function() {
            $('#search').on('keyup', function () {
                $value = $(this).val();
                if($value===''){
                    $('#display').empty();
                }
                else {
                    $.ajax({
                        type: 'get',
                        url: '{{route("search.post")}}',
                        data: {'search': $value},
                        success: function (data) {
                            console.log(data);
                            // if($value==='') {
                            //     $('#display').html('No Search Results').show()
                            // }
                            // else{
                                $('#display').html(data).show();
                            // }
                        },
                        error: function (data) {
                            console.log(data)
                        }
                    });
                }
            })
        });
    </script>
    <script>
        $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
    </script>
    @stop
