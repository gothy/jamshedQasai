@extends('admin_UI.layouts.master')
@section('custom_head')
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
@stop
@section('content')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container">
                <div class="w3-center"><br>
                    <img src="{{asset('admin_UI/img/img_avatar4.png')}}" alt="Avatar" style="width:30%" class="w3-circle w3-margin-top">
                </div><br>
                <form method="POST" action="{{ route('update_Permission') }}">
                    @csrf
                    <input type="hidden" value="{{$permission->id}}" name="permissionId">
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{$permission->name}}" required autofocus>

                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('name') }}</strong>
                                                        </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Update') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop