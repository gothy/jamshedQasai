<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>Stripe</title>
    <link href="{{URL::to('blog_UI/stripe/stripe.css')}}" rel="stylesheet" >
</head>
<body>
<script src="https://js.stripe.com/v3/"></script>
<form action="{{route('payment')}}" method="post" id="payment-form">
    @csrf
    <div class="form-row">
        <label for="card-element">
            Credit or debit card
        </label>
        <div id="card-element">
            <!-- A Stripe Element will be inserted here. -->
        </div>
        <!-- Used to display form errors. -->
        <div id="card-errors" role="alert"></div>
    </div>

    <button>Submit Payment</button>
</form>
<script src="{{URL::to('blog_UI/stripe/stripe.js')}}"></script>
</body>
</html>