@extends('layouts.master')

@section('head_section')
    <link rel="stylesheet" href="{{URL::to('blog_css/dropify/demo.css')}}">
    <link rel="stylesheet" href="{{URL::to('blog_css/dropify/dropify.min.css')}}">
    @stop

@section('extra_scripts')
    <script src="{{URL::to('blog_js/jquery/jquery.min.js')}}"></script>
    <script src="{{URL::to('blog_js/dropify/dropify.min.js')}}"></script>
    <script>
        $(document).ready(function(){
            $('.alert-success').fadeIn().fadeOut(3000);
            // Basic
            $('.dropify').dropify();

            // Used events
            var drEvent = $('#input-file-events').dropify();

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e){
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            })
        });
    </script>
    @stop
@section('content')
    <header class="masthead" data-default-file="">
        <div class="overlay"></div>
        <div class="container">
            {{--<div class="row">--}}
                {{--<div class="col-lg-8 col-md-10 mx-auto">--}}
                    {{--<div style="text-align: center" class="post-heading">--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </header>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Edit Profile<a href="{{route('home')}}"><div class="float-right">Home</div></a></div>
                    <div class="card-body">
                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                        @endif
                        <form method="POST" action="{{ route('updateProfile') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $user->name}}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $user->email }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Old Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="oldpassword" required placeholder="Type Your Old Password Janab">

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('New Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="newpassword" placeholder="Type Your New Password Janab">

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            @if($errors->has('g-recaptcha-response'))
                                <span class="invalid-feedback" style="display: block">
                                    <strong>{{$errors->first('g-recaptcha-response')}}</strong>
                                </span>
                            @endif
                            <div class="g-recaptcha"
                                 data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}">

                            </div><br>
                            @if(@Auth::user()->path == null)
                            <div class="row">
                                @if($errors->has("pic"))
                                    <p>{{$errors->first("pic")}}</p>
                                @endif
                                <div class="col-sm-12">
                                    <label for="input-file-now">Select Image</label>
                                    <input type="file" id="input-file-now" class="dropify" name="pic"/>
                                    <br />
                                </div>
                            </div>
                            @else
                                <div class="row">
                                    @if($errors->has("pic"))
                                        <p>{{$errors->first("pic")}}</p>
                                    @endif
                                    <div class="col-sm-12">
                                        <label for="input-file-now">Select Image</label>
                                        <input type="file" id="input-file-now" class="dropify" name="pic"
                                               data-default-file="{{URL::to($user->path)}}"/>
                                        <br />
                                    </div>
                                </div>
                            @endif
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Update
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
