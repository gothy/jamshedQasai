@extends('layouts.master')

@section('head_section')
    <link rel="stylesheet" href="{{URL::to('blog_css/css/image.css')}}">
@stop

@section('content')

    <!-- Page Header -->
    <header class="masthead" style="background-image: url('{{URL::to($blog->path)}}')" data-default-file="">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    <div style="text-align: center" class="post-heading">
                        <h1>{{$blog->title}}</h1>
                        <span class="meta">Posted by
                            {{$blog->postedBy['name']}} on
                            {{date('d-m-Y', strtotime($blog->created_at))}}
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Post Content -->
    <article>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    {!!($blog->content)!!}
                </div>
                <div class="col-md-12">
                    <h1>Gallery</h1>
                    <hr>
                    <div class="row">
                        @foreach($blog->imagesOfPost as $image)
                            <div class="col-md-3">
                            <img style="cursor:zoom-in" src="{{asset($image->path)}}" width="100%"
                                 onclick="document.getElementById('modal01').style.display='block'">
                                <div id="modal01" class="w3-modal" onclick="this.style.display='none'">
                                    <span class="w3-button w3-hover-red w3-xlarge w3-display-topright">&times;</span>
                                    <div class="w3-modal-content w3-animate-zoom">
                                        <img src="{{asset($image->path)}}" style="width:100%">
                                    </div>
                                </div>
                            </div>
                        </div>
                            @endforeach
                    </div>
                </div>
            </div>
        </div>
    </article>

    <hr>
    <!-- Comments Form -->
    <div class="container comment-box">
        @Auth
            <div class="card col-md-12">
                <h5 class="card-header">Leave a Comment:</h5>
                <div class="card-body">
                    <div>
                        @csrf
                        <input type="hidden" value="{{$blog->id}}" name="blogId">
                        <div class="form-group">
                            <textarea class="form-control" rows="3" id="comment"></textarea>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary btn-submit">Submit</button>
                        </div>
                    </div>
                </div>

            </div>
            <br>
        @else
            <a href="{{route('login')}}"><button class="btn btn-primary col-md-12">Sign In To Comment</button></a><br>
        @endAuth
        <br>
        <!-- Single Comment -->
        @foreach($blog->blogComment as $comment)
            <div class="media col-md-12">
                <img class="d-flex mr-3 rounded-circle" width="5%" src="{{asset($comment->commentFromUser->path)}}" alt="Image Unavailable">
                <div class="media-body">
                    <h5 class="mt-0">
                        {{ $comment->commentFromUser->name }}
                    </h5>
                    {{$comment->comment}}
                </div>
            </div>
            <br>
            {{--@endforeach--}}
        @endforeach
    </div>
@stop
@section('extra_scripts')
    <script>
        $(document).ready(function(){
            $(".btn-submit").click(function(){
                var token = $("meta[name=csrf-token]").attr('content');
                $.ajax({
                    url: '{{route("add.comment")}}',
                    method: "post",
                    data:{
                        _token: token,
                        comment : $('#comment').val(),
                        blog_id : $('input[name=blogId]').val()
                    },
                    success : function(data){
                        console.log(data)
                        if(data.status==200){
                            $('#comment').val('')
                            $('.comment-box').append('<div style="display:none" class="new-comment media col-md-12"><img class="d-flex mr-3 rounded-circle" width="90px" src="/'+data.image+'" alt=""><div class="media-body"><h5 class="mt-0">'+data.user+'</h5>'+data.comment.comment+'</div></div><br>')
                            $('.new-comment').fadeIn()
                        }
                    },
                    error : function(err){
                        console.log(err)
                    }
                })
            });
        });
    </script>
@stop