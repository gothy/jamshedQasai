@extends('admin_UI.layouts.master')
@section('custom_head')
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
@stop
@section('content')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container">
                <div class="w3-center"><br>
                    <img src="{{asset('admin_UI/img/img_avatar4.png')}}" alt="Avatar" style="width:30%" class="w3-circle w3-margin-top">
                </div><br>
                <form method="POST" action="{{ route('add_Role') }}">
                    @csrf
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" required autofocus>

                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('name') }}</strong>
                                                        </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="role" class="col-md-4 col-form-label text-md-right">Roles</label>
                        <div class="checkbox">
                            @foreach($permissions as $permission)
                                <label>
                                    <input type="checkbox" value="{{$permission->id}}" name="permission[]">{{$permission->name}}
                                </label>
                        </div>
                        @endforeach
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Create') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop