<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand" href="{{route('blogMain')}}">Start Bootstrap</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{route('blogMain')}}">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('blogAbout')}}">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('blogContact')}}">Contact</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('stripe')}}">Stripe</a>
                </li>
                <li class="nav-item dropdown">
                    @auth
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                @else
                    <li class="nav-item">
                        <a href="{{route('login')}}" class="nav-link">
                            <span class="caret">Login</span>
                        </a></li>
                    <li class="nav-item">
                        <a href="{{route('register')}}" class="nav-link">
                            <span class="caret">Register</span>
                        </a>
                    </li>
                @endauth
                @Auth
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('editProfile',@Auth::user()->id)}}">
                        Edit Profile
                    </a>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                    @endauth
                </div>
            </ul>
        </div>
    </div>
</nav>