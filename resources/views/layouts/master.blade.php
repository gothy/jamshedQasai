<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    @include('layouts.header')
    <body>
        @include('layouts.navbar')
            <main>
                @yield('content')
            </main>
        @include('layouts.footer')
        @include('layouts.scripts')
    </body>
</html>
