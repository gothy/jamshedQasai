

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="{{URL::to('admin_UI/jquery/jquery.min.js')}}"></script>
<script src="{{URL::to('blog_js/dropify/dropify.min.js')}}"></script>
{{--<!-- Bootstrap 4 -->--}}
<script src="{{URL::to('admin_UI/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
{{--<!-- AdminLTE App -->--}}
<script src="{{URL::to('admin_UI/js/adminlte.min.js')}}"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-134371525-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-134371525-1');
</script>


@yield('scripts')