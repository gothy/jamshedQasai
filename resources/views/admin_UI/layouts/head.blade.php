    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Khadim Starter Pack</title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{URL::to('admin_UI/font-awesome/css/font-awesome.min.css')}}">
    {{--<!-- Theme style -->--}}
    <link rel="stylesheet" href="{{URL::to('admin_UI/css/adminlte.min.css')}}">
    {{--<!-- Google Font: Source Sans Pro -->--}}
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link rel="stylesheet" href="{{URL::to('blog_css/dropify/demo.css')}}">
    <link rel="stylesheet" href="{{URL::to('blog_css/dropify/dropify.min.css')}}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-lite.css" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">


    @yield('custom_head')
