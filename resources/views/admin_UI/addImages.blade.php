@extends('admin_UI.layouts.master')
@section('custom_head')
    <link rel="stylesheet" href="https://rawgit.com/enyo/dropzone/master/dist/dropzone.css">
@stop

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
    <!-- Page Header -->
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Multiple Image Upload') }}</div>
                    <div class="card-body">
                        <form action="{{ route('multi.upload') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" value="{{$blog_id}}" name="blog_id">
                            <div id="myId" class="fallback dropzone">
                            </div>
                            <br/>
                            <button class="btn btn-block btn-primary"> Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
        </div>
    </div>
    @stop

@section('scripts')
    <script src="https://rawgit.com/enyo/dropzone/master/dist/dropzone.js"></script>
    <script>
        Dropzone.autoDiscover = false;
    </script>
    <script>
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $("div#myId").dropzone({

            url: '{{route("multi.upload")}}',
            paramName: "image",
            headers: {
                'X-CSRF-TOKEN': CSRF_TOKEN
            },
            init: function() {
                this.on("sending", function(file, xhr, formData){
                    formData.append("blog_id", '{{$blog_id}}')
                })
            }
        });

    </script>
    @stop