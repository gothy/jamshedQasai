@extends ('admin_UI.layouts.master')

@section('custom_head')
<link rel="stylesheet" href="{{URL::to('admin_UI\datatables\dataTables.bootstrap4.css')}}">

@stop

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">All Blogs</h3>
                    </div>
                    <div class="card-body">
                        <div id="example1_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="example1" class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Title</th>
                                            <th>Image</th>
                                            <th>Edit/Delete</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($allBlogs as $blog)
                                            <tr>
                                                <td>{{$blog->id}}</td>
                                                <td>{{$blog->title}}</td>
                                                <td style="text-align: center"><img width="40px" src="{{asset($blog->path)}}"></td>
                                                <td><a href="{{route('edit',$blog->id)}}">
                                                        <button class="btn btn-primary" value="{{$blog->id}}">
                                                            Edit
                                                        </button>
                                                    </a>
                                                    <a href="{{route('delete',$blog->id)}}">
                                                        <button class="btn btn-danger" value="{{$blog->id}}">
                                                            Delete
                                                        </button>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @stop
@section('scripts')
    <script src="{{URL::to('admin_UI/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{URL::to('admin_UI/datatables/dataTables.bootstrap4.js')}}"></script>

    <script>
        $(document).ready( function () {
            $('#example1').DataTable();
        } );
    </script>
@stop
