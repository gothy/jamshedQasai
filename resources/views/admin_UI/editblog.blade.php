@extends('admin_UI.layouts.master')

@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Update Blog</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Starter Page</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
                <hr><br>
        <form method="post" action="{{route('update')}}" enctype="multipart/form-data">
            @csrf
            <div class="card">
                <div class="card-body">
                    <input value="{{$blog->id}}" name="blogId" hidden>
                    <div class="card-title" style="color: blue;">Heading</div>
                <input type="text" name="title" value="{{$blog->title}}"><br><br>
                    <textarea id="summernote" name="summernoteInput">{{$blog->content}}</textarea>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-6">
                                <label for="input-file-now">Select Image</label>
                                <input type="file" id="input-file-now" class="dropify" name="pic"
                                       data-default-file="{{URL::to($blog->path)}}"/>
                                <br />
                            </div>
                            <div class="col-sm-3"></div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-dark float-sm-right">Save</button>
                </div>
            </div>
        </form>
            </div><!-- /.container-fluid -->
        </div>
    </div>
@stop
    @section('scripts')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-lite.js"></script>
        <script>
            $('#summernote').summernote({
                placeholder: 'Type Something Nigga....',
                tabsize: 2,
                height: 200
            });
        </script>
    <script>
        $(document).ready(function(){
            // Basic
            $('.dropify').dropify();

            // Used events
            var drEvent = $('#input-file-events').dropify();

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e){
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            })
        });
    </script>
    @stop
