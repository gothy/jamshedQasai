<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'add_blog',
            'edit_blog',

        ];
        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission,
                'guard_name'=>'admin']);
        }

//        $role = Role::create(['name' => 'admin',
//            'guard_name'=>'admin']);
//        $role->givePermissionTo(Permission::all());
//        $gamer=\App\Admin::find(1);
//
//        $role_r = Role::where('id', '=', 1)->firstOrFail();
//        $gamer->assignRole($role_r);
    }
}
