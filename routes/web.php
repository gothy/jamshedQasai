<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('mainPage');
});

Auth::routes();

/**Navigation Routes for Admin**/

Route::prefix('admin')->group(function () {
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/', 'AdminBlogController@index')->name('admin_panel');
    Route::get('/viewAdmins','AdminController@viewAdmins')->name('viewAdmins');
    Route::get('/addAdmin','AdminController@addAdmin')->name('addAdmin');
    Route::post('/registerAdmin','AdminController@registerAdmin')->name('register_admin');
    Route::get('/editAdmin,{id}','AdminController@editAdmin')->name('edit_admin');
    Route::post('/updateAdmin','AdminController@updateAdmin')->name('update_admin');

    Route::get('/viewRoles','AdminController@viewRoles')->name('view_Roles');
    Route::get('/newRole','AdminController@newRole')->name('new_Role');
    Route::post('/addRole','AdminController@addRole')->name('add_Role');
    Route::get('/editRole,{id}','AdminController@editRole')->name('edit_Role');
    Route::post('/updateRole','AdminController@updateRole')->name('update_Role');

    Route::get('/assignRole,{id}','AdminController@assignRole')->name('assign_Role');
    Route::post('/updateAssignedRole','AdminController@updateAssignedRole')->name('updateAssignedRole');



    Route::group(['middleware' => ['permission:add_blog']], function () {
        Route::get('/addblog', 'AdminBlogController@blogCreator')->name('addblog');
        Route::post('/newPost', 'AdminBlogController@store')->name('newPost');
        Route::post('/updateImages', 'AdminBlogController@updateMultipleImages')->name('multi.upload');
        Route::get('/allPosts', 'AdminBlogController@show')->name('allPosts');
    });
    Route::group(['middleware' => ['permission:edit_blog']], function () {
        Route::get('/allPosts', 'AdminBlogController@show')->name('allPosts');
        Route::get('/editPost/{id}', 'AdminBlogController@edit')->name('edit');
        Route::post('/update', 'AdminBlogController@update')->name('update');
    });
    Route::get('/delete/{id}', 'AdminBlogController@destroy')->name('delete');
});



/**Navigation Routes For User**/
Route::get('/', 'BlogController@index')->name('home');
Route::get('/index', 'BlogController@index')->name('blogMain');
Route::get('/blogPost/{id}', 'BlogController@blogPost')->name('blogPost');
Route::get('/blogAbout','BlogController@blogabout')->name('blogAbout');
Route::get('/stripe','BlogController@stripe')->name('stripe');
Route::post('/payment','BlogController@payment')->name('payment');
Route::get('/editProfile/{id}','BlogController@editProfile')->name('editProfile');
Route::post('/updateProfile','BlogController@update')->name('updateProfile');
Route::get('/blogContact','BlogController@blogContact')->name('blogContact');
Route::post('/comment','BlogController@addComment')->name('add.comment');
Route::post('/mail','BlogController@send')->name('send.mail');
Route::get('/search','BlogController@search')->name('search.post');
Route::get('/elasticsearch','BlogController@setElastic');

/**Analytics Data**/
Route::get('/analytics','AnalyticsController@testData');
Route::get('/graphs','AnalyticsController@countryData');


