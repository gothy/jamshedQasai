<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



    Route::get('/register', 'Api\AuthUserController@register');
    Route::get('/login', 'Api\AuthUserController@login');

    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('/showBlog', 'Api\ApiBlogController@showBlog');
        Route::get('/showBlogs', 'Api\ApiBlogController@showBlogs');
        Route::get('/showBlog', 'Api\ApiBlogController@showBlog');
        Route::get('/addComment', 'Api\ApiBlogController@addComment');
        Route::get('/details', 'Api\AuthUserController@details');
    });


